using HeroDigitalMVC.Utilities;
using NUnit.Framework;

namespace HeroDigitalUnitTest
{
    public class EmailVilidation_Tests
    {
        [TestCase("darwin@shower.com", true)]
        [TestCase("121@gmail.com", true)]
        [TestCase("darwin@internation.com.vn", true)]
        [TestCase("12@sh@owercom", false)]
        [TestCase("darwin@sh@ower.com", false)]
        [TestCase(".darwin@shower.com", false)]
        [TestCase("@darwin@sh@ower.com", false)]
        [TestCase("darwin@sh@ower.com!", false)]
        public void ValidEmail(string email, bool expectResult)
        {
            Assert.AreEqual(expectResult, EmailValidation.IsValidGlobalEmail(email));
        }
    }
}