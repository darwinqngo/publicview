using Microsoft.AspNetCore.Mvc.Rendering;
using HeroDigitalMVC.Utilities;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace HeroDigitalUnitTest
{

    public class Age_Tests
    {
        static List<SelectListItem> ageListData = new List<SelectListItem>();

        [SetUp]
        public void Setup()
        {
            ageListData = AgeList.Get(selectedValue: "10");
        }
        /// <summary>
        /// Test to see if we have data from age list.
        /// </summary>
        [Test]
        public void AgeList_CountData()
        {
            Assert.Greater(ageListData.Count, 1);
        }
        /// <summary>
        /// Test to see if age 10 has data
        /// </summary>
        [Test]
        public void AgeList_TestAge_10()
        {
            var countAge = AgeList.Get("10").Count(w => w.Text == "10");
            Assert.AreEqual(1, countAge);
        }
        /// <summary>
        /// Test to see if we have any duplicate data
        /// </summary>
        [Test]
        public void AgeList_TestDuplicate()
        {
            var duplicate = ageListData.Select(s => s.Text).GroupBy(x => x).Any(g => g.Count() > 1);
            Assert.AreEqual(false, duplicate);
        }
        /// <summary>
        /// Test convert key to real age
        /// </summary>
        [Test]
        public void AgeList_TestConvertKeyToAge29()
        {
            var age29Test = AgeList.GetConvertKeyToValue("eD221");
            Assert.AreEqual("29", age29Test);
            var age29Test2 = AgeList.GetConvertKeyToValue("R8JTk");
            Assert.AreEqual("29", age29Test2);
        }
    }
}