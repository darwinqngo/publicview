﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestDotNetCoreMvc.Models
{
    public class Contact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        [Required]
        [Display(Name = "First Name:", Prompt = "Enter First Name")]
        [MaxLength(120)]
        public string firstName { get; set; }

        [Required]
        [Display(Name = "Last Name:", Prompt = "Enter Last Name")]
        [MaxLength(120)]
        public string lastName { get; set; }

        [Required]
        [Display(Name = "Email Addresss:", Prompt = "Enter Email Address")]
        [MaxLength(500)]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number:", Prompt = "Enter Phone Number")]
        [MaxLength(500)]
        public string phone { get; set; }

        [Required]

        [Display(Name = "Age:", Prompt = "Select Age")]
        [MaxLength(20)]
        public string age { get; set; }

        [NotMapped]//Don't map to database
        [Display(Name = "Age:", Prompt = "Select Age")]
        public List<SelectListItem> ageOptions { get; set; }

        [Required]
        [Display(Name = "Your Message:", Prompt = "Your Message")]
        public string message { get; set; }

        /// <summary>
        /// Help to design cookiesless and session less.
        /// </summary>
        public string UserToken { get; set; }

    }
}
