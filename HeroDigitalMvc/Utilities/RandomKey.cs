﻿using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroDigitalMVC.Utilities
{
    public class RandomKey
    {
        /// <summary>
        /// Generate random string and convert to base 64
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenateBase64UrlEncode(int length)
        {
            var rand = new Random();
            string randomData = string.Join("", Enumerable.Repeat(0, length).Select(n => (char)rand.Next(length)));
            return WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(randomData));
        }
    }
}
