﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HeroDigitalMVC.Utilities
{
    public class EmailValidation
    {
        /// <summary>
        /// Internation email validation, this wil able to support internation format instead of  ISO basic Latin alphabet
        //Only allow 1 @ character
        //Validate email with at least 1 dot
        //The first and last character can't equal to . or @
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidGlobalEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }
            return
                email.Count(c => c == '@') == 1 //must only have 1 @
                && email.Contains(".") //allow multiple .
                && email.First() != '.' //the first char can't equal to .
                && email.First() != '@' //the first char can't email to @
                && email.Last() != '.'//the last char can't equal to .
                && email.Last() != '@';//the last char can't equal to .
        }
    }
}
