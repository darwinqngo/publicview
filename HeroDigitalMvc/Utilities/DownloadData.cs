﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace HeroDigitalMVC.Utilities
{
    public class DownloadData
    {
        /// <summary>
        /// Download data using get method
        /// </summary>
        /// <param name="url"></param>
        /// <param name="maxLoop"></param>
        /// <param name="waitMillisecondsIfSomethingWrong"></param>
        /// <param name="timeOutMiliseconds"></param>
        /// <returns></returns>
        public static string Get(string url, int maxLoop = 3, int waitMillisecondsIfSomethingWrong = 10000, int timeOutMiliseconds = 10000)
        {
            do
            {
                try
                {
                    HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(url);
                    wReq.AllowAutoRedirect = true;
                    wReq.KeepAlive = false;
                    wReq.Timeout = timeOutMiliseconds;
                    wReq.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    //Request format text/html. Will improve this if nessary Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
                    wReq.ServicePoint.Expect100Continue = true;
                    wReq.ContentLength = 0;
                    using (HttpWebResponse response = (HttpWebResponse)wReq.GetResponse())
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                return reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    if (maxLoop == 0)
                    {
                        return null;
                    }
                }
                maxLoop = maxLoop - 1;
                //Wait n milliseconds and download gain
                System.Threading.Thread.Sleep(waitMillisecondsIfSomethingWrong);
            } while (maxLoop > 0);

            return null;
        }
    }
}
