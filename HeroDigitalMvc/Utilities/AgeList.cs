﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HeroDigitalMVC.Utilities
{
    public class AgeList
    {
        private const string url = "https://coderbyte.com/api/challenges/json/age-counting";
        public static List<SelectListItem> Get(string selectedValue)
        {
            List<SelectListItem> ageList = new List<SelectListItem>();
            if (string.IsNullOrEmpty(selectedValue))
            {
                ageList.Add(new SelectListItem { Text = "", Value = "", Selected = true });
            }
            var ageStr = DownloadData.Get(url);
            if (ageStr == null) { return ageList; }
            //Clean messy json data and convert data into string array
            var ageArr = ageStr.Replace(@"{""data"":""", "").Replace("\"}", "").Replace(" ", "").Replace("key=", "").Replace("age=", "").Split(',');
            //Convert data to list 
            List<ageKey> openWith = new List<ageKey>();
            for (int i = 0; i < ageArr.Count(); i = i + 2)
            {
                openWith.Add(new ageKey { key = ageArr[i], age = ageArr[i + 1] });
            }
            //For duplicate ages, use the option with the highest key / age index in the entire data
            //Order age
            var q1 = (from a in openWith.Select(s => s.age).Distinct().OrderBy(o => o.Length).ThenBy(o => o)
                      select new ageKey
                      {
                          age = a,
                          key = openWith.Where(w => w.age == a).OrderByDescending(o => o.key.Length).ThenByDescending(o => o.key).First().key
                      }).ToList();

            for (int i = 0; i < q1.Count(); i = i + 2)
            {
                ageList.Add(new SelectListItem { Text = q1[i].age, Value = q1[i].key, Selected = (q1[i].key == selectedValue) ? true : false });
            }

            return ageList.ToList();
        }
        public static string GetConvertKeyToValue(string ageKey)
        {
            var ageStr = DownloadData.Get(url);
            if (ageStr == null) { return ""; }
            //Clean messy json data and convert data into string array
            var ageArr = ageStr.Replace(@"{""data"":""", "").Replace("\"}", "").Replace(" ", "").Replace("key=", "").Replace("age=", "").Split(',');
            //Convert data to list 
            List<ageKey> openWith = new List<ageKey>();
            for (int i = 0; i < ageArr.Count(); i = i + 2)
            {
                if (ageArr[i] == ageKey)
                {
                    return ageArr[i + 1];
                }
            }
            return "";
        }


        class ageKey
        {
            public string age { get; set; }
            public string key { get; set; }

        }
    }
}
