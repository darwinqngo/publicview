Run project using Visual Studio 2019

Framework: .NET 5.0

Setup Database

	System will auto create database if it has the right credential.

Update connection string if want to run on the web server.

To go appsettings.json and update the value for DefaultConnection

Update database manually only if you are making changes to schema.

	add-migration "Your Comment" -Context ApplicationDbContext

	update-database -Context ApplicationDbContext
