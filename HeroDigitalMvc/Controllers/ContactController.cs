﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using HeroDigitalMVC.Data;
using HeroDigitalMVC.Utilities;
using TestDotNetCoreMvc.Models;

namespace HeroDigitalMVC.Controllers
{
    public class ContactController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ContactController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Contact/Details/5
        public async Task<IActionResult> Details(long? id, string UserToken)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _context.Contact
                .FirstOrDefaultAsync(m => m.Id == id);
            if (contact == null
                //Validate id with token for security purposes
                || (contact != null && contact.UserToken != UserToken)
                )
            {
                return NotFound();
            }
            //Convert age key to number
            contact.age = "Key: " + contact.age + " Value:" + AgeList.GetConvertKeyToValue(contact.age);
            return View(contact);
        }

        // GET: Contact
        public IActionResult Index()
        {
            Contact contact = new Contact();
            contact.ageOptions = AgeList.Get(null);
            return View(contact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([Bind("firstName,lastName,email,phone,age,message")] Contact contact)
        {
            try
            {
                contact.ageOptions = AgeList.Get(contact.age);
                if (ModelState.IsValid)
                {
                    if (EmailValidation.IsValidGlobalEmail(contact.email) == false)
                    {
                        ModelState.AddModelError(nameof(contact.email), "Invalid email address!");
                        return View(contact);
                    }
                    contact.UserToken = RandomKey.GenateBase64UrlEncode(128);
                    _context.Add(contact);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Details", "Contact", new { id = contact.Id, contact.UserToken });
                }
            }
            catch (Exception ex)
            {
                ViewData["Error"] = "Unable to sumbit your request, please try again!";

                return View(contact);
            }
            return View(contact);
        }
    }
}
