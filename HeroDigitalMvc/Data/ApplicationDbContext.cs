﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using TestDotNetCoreMvc.Models;

namespace HeroDigitalMVC.Data
{

    //Notes if you want use asp.net identity tables use IdentityDbContext instead of the DbContext
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<Contact> Contact { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(null);
            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasIndex(e => e.Id);
                entity.Property(e => e.firstName).IsRequired();
                entity.Property(e => e.lastName).IsRequired();
                entity.Property(e => e.email).IsRequired();
                entity.Property(e => e.phone).IsRequired();
                entity.Property(e => e.age).IsRequired();
            });

        }
    }
}
